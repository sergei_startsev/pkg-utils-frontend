﻿var Logger = {
    log: function (msg) {
        /// <summary>
        /// Log a message to the browser console if GlobalApp.debug is true (it uses console.log)
        /// </summary>
        /// <param name="msg" type="String">Message</param>
        if (GlobalApp.debug && typeof (console) != "undefined") {
            var logStyle = "color:#1E91FC;font-weight:bold;";
            var stackTraceStyle = "color:#555;font-size:.9em;";

            console.log("%c" + msg, logStyle);

            try { throw Error('') } catch (err) {
                if (typeof (err.stack) != "undefined") {
                    var caller_line = err.stack.split("\n")[3];
                    console.log("%c" + caller_line, stackTraceStyle);
                }
            }
        }
    },

    debug: function (obj) {
        /// <summary>
        /// Show an object in the browser console if GlobalApp.debug is true (it uses console.debug)
        /// </summary>
        /// <param name="obj" type="Object">Object</param>
        if (GlobalApp.debug && typeof (console) != "undefined") {
            var stackTraceStyle = "color:#555;font-size:.9em;";

            console.debug(obj);

            try { throw Error('') } catch (err) {
                if (typeof (err.stack) != "undefined") {
                    var caller_line = err.stack.split("\n")[3];
                    console.log("%c" + caller_line, stackTraceStyle);
                }
            }
        }
    }
};

var TemplateManager = {
    templates: {},

    get: function (path) {
        /// <summary>
        /// Load or get loaded template resource
        /// </summary>
        /// <param name="path" type="String">Full template path</param>
        var template = this.templates[path];
        if (template) {
            return template;

        } else {
            var that = this;
            $.ajax({
                url: path,
                method: 'GET',
                async: false,
                cache: false,
                success: function (template) {
                    that.templates[path] = _.template(template);
                }
            });
            return this.templates[path];
        }
    }

};

var ErrorProcessor = {
    showErrorOnPage: function (xhr, $el) {
        /// <summary>
        /// Paste the error message to the page
        /// </summary>
        /// <param name="xhr">XMLHttpRequest object</param>
        /// <param name="$el" type="JQuery">page element</param>

        //Redirect for ajax requests with code 401
        if (xhr.status == 401) {
            Backbone.trigger("authModalWindowView:show");
            return false;
        }

        var errorMessage = "<div class='error-message-text'>";
        errorMessage += "<b>Произошла ошибка при запросе данной страницы...</b>";
        errorMessage += "<br/>";
        errorMessage += "<br/>";
        errorMessage += "Ошибка: " + xhr.responseText;
        errorMessage += "<br/>";
        errorMessage += "Код: " + xhr.status + " " + xhr.statusText;
        errorMessage += "<br/>";
        errorMessage += "<br/>";
        errorMessage += "<p style='color:black;'>";
        errorMessage += "Что делать дальше:";
        errorMessage += "<br/>";
        errorMessage += "- <span class='no-data-refresh' title='Обновить'><a>обновить страницу</a></span> " +
        "<span class='no-data-refresh' title='Обновить'><i class='k-icon k-i-refresh'></i></span>," +
        " возможно, проблема была временной";
        errorMessage += "<br/>";
        errorMessage += "- если вы вводили адрес страницы вручную, проверьте его корректность";
        errorMessage += "<br/>";
        errorMessage += "- мы оповещены о вашей проблеме и сделаем все, чтобы это не произошло вновь.";
        errorMessage += "</p>";
        errorMessage += "</div>"

        $el.html(errorMessage);

        /*$('html, body').animate({
            scrollTop: $el.offset().top
        }, 1000);*/
    }
}

var DateTimeUtil = {
    doubleToDateTime: function (doubleTime) {
        /// <summary>
        /// Convert double to Date
        /// </summary>
        /// <param name="doubleTime">Date in double format</param>
        /// <returns type=""></returns>
        if (typeof doubleTime !== "undefined" && doubleTime != null) {
            //if value is zero return empty string
            if (doubleTime == 0) {
                return "";
            }

            var date1 = new Date("1899-12-30");
            var time1 = date1.getTime();
            var date2 = new Date("1970-01-01");
            var time2 = date2.getTime();

            var timezoneOffset = date1.getTimezoneOffset() * 1000 * 60;

            //1970-01-01 == 0 mlsc
            if (time2 != 0) {
                time2 -= timezoneOffset;
                time1 -= timezoneOffset;
            }

            var res = new Date(time2 + Math.round(86400 * parseFloat(doubleTime) * 1000) + time1 +
                timezoneOffset);

            return res;
        } else {
            return new Date("1899-12-30");
        }
    },
    dateTimeToDouble: function (date) {
        /// <summary>
        /// Convert Date to double
        /// </summary>
        /// <param name="date" type="Date()">Date</param>
        /// <returns type="double">Date in double format</returns>
        var date1 = new Date("1899-12-30");

        var res = - date1.getTime() + date.getTime();
        res = res/(86400 * 1000);

        return res;
    }
}

var Cloner = {
    clone: function (obj) {
        /// <summary>
        /// Clone object with all its properties including nesting objects
        /// </summary>
        /// <param name="obj">cloneable object</param>
        /// <returns type="object">clone of the object</returns>
        if (obj == null || typeof (obj) != 'object')
            return obj;

        var temp = obj.constructor(); // changed

        for (var key in obj)
            temp[key] = Cloner.clone(obj[key]);
        return temp;
    }
}

var HtmlDecoder = {
    decode: function (str) {
        /// <summary>
        /// Decode special chars to Html symbols
        /// </summary>
        /// <param name="str">string to decode</param>
        /// <returns type="string">decoded string</returns>
        if (typeof str !== "undefined" && str!= null) {
            return _.escape(str);
        } else {
            return "";
        }
    }
}
