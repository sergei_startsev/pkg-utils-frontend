﻿//**WEB_DEV team** (Центр биржевых информационных технологий)

//- [Sergei Startsev](https://bitbucket.org/sergei_startsev)

// ### JS Checks Browser Support.

function BrowserSupport(options) {
    //public fileds
    var defaultOptions = {
        //Error messages
        changeBrowserMessage: "Для работы с системой установите последнюю версию браузера.",
        fileAPIErrorMessage: "- Ваш браузер не поддерживает работу с файловой системой (HTML5 File API)\n\n",
        CORSErrorMessage: "- Ваш браузер не поддерживает работу с кросс-доменными запросами (Cross-origin resource sharing)\n\n",

        showMessage: function (message) {
           alert(message);
        }
    };

    for (var option in defaultOptions) {
        this[option] = options && options[option] !== undefined ? options[option] : defaultOptions[option];
    }

    this.checkBrowserSupport = function () {
        /// <summary>
        /// Checks browser features support that are required by our app
        /// </summary>
        /// <param name=""></param>

        var message = "", isValid = true;

        if (!isFileAPISupported.call(this)) {
            message += this.fileAPIErrorMessage;
            isValid = false;
        }

        if (!isCORSSupported.call(this)) {
            message += this.CORSErrorMessage;
            isValid = false;
        }

        if (!isValid) {
            message += this.changeBrowserMessage;
            this.showMessage(message);
            return false;
        } else {
            return true;
        }
    }

    function isFileAPISupported() {
        /// <summary>
        /// Checks File API browser support
        /// </summary>
        /// <returns type="boolean">true - support, false - no</returns>

        return window.File && window.FileReader ? true : false;
    }

    function isCORSSupported() {
        /// <summary>
        /// Checks CORS Requests browser support
        /// </summary>
        /// <returns type="boolean">true - support, false - no</returns>

        if ("withCredentials" in new XMLHttpRequest())
            return true;
        else if (window.XDomainRequest)
            return true;
        else
            return false;
    }
};