﻿function TmplEngine(options) {
    //private fields
    var reservedWords = {
        "PrefixStart": "{{",
        "PrefixEnd": "}}",
        "PrefixStartValue": "=",
        "PrefixEndValue": "",
        "PrefixOpenStartLoop": "each",
        "PrefixOpenEndLoop": "",
        "PrefixCloseStartLoop": "/each",
        "PrefixCloseEndLoop": "",
        "PrefixVariable": "[$]",
        "PrefixIndex": "index"
    };

    //public fileds
    var defaultOptions = {
        showMessage: function (message) {
            
        }
    };

    for (var option in defaultOptions) {
        this[option] = options && options[option] !== undefined ? options[option] : defaultOptions[option];
    }

    this.generateDocument = function (tmpl, dict) {
        /// <summary>
        /// Функция генерации документа по шаблону
        /// </summary>
        /// <param name="tmpl">Содержимое шаблона</param>
        /// <param name="dict">Словарь для заполнеиня шаблона</param>
        /// <returns type="string">Документ сформированный по шаблону</returns>
        
        //текущий индекс позиции, по которой осуществляется навигация
        var currentIndex = 0;
        //индекс начала тега
        var startIndex = 0;
        //индекс конца тега
        var endIndex = 0;
        //признак цикла
        var loop = false;

        //проходимся по шаблону в поиске цикла
        do {
            currentIndex = tmpl.indexOf(reservedWords.PrefixStart, currentIndex);
            //продолжаем, если найден признак начала тега
            if (currentIndex != -1) {
                startIndex = currentIndex;
                endIndex = tmpl.indexOf(reservedWords.PrefixEnd, currentIndex) + reservedWords.PrefixEnd.length;

                //определяем название тега
                var strTag = tmpl.substring(currentIndex, endIndex);

                //этот тег цикл?
                loop = isLoop.call(this, strTag);

                currentIndex = endIndex;

                //если наткнулись на цикл
                if (loop) {
                    //получаем имя тега, например, в теге {{each Deal}} это будет Deal
                    var tagName = getTagName.call(this, strTag);
    
                    //если цикл, то ищем позицию закрывающего тега
                    endIndex = tmpl.indexOf(reservedWords.PrefixStart + reservedWords.PrefixCloseStartLoop +
                        " " + tagName + reservedWords.PrefixCloseEndLoop + reservedWords.PrefixEnd,
                            endIndex);
    
                    //если нашли закрывающий тег цикла
                    if (endIndex != -1) {
                        //выделяем блок цикла
                        var loopBlock = tmpl.substring(currentIndex, endIndex);
                        //здесь будет конечный результат вставки данных в блок цикла
                        var resultLoopBlock = "";
                        
                        //находим в словаре значение по указанному ключу
                        if (dict[tagName] != undefined) {
                            for (var i = 0; i < dict[tagName].length; i++) {
                                var loopBlockItem = loopBlock;
                                //проходимся по ключам элемента массива
                                for (var key in dict[tagName][i]) {
                                    //и заменяем теги с ключами на значения
                                    loopBlockItem = loopBlockItem.replace(
                                        new RegExp(
                                            reservedWords.PrefixStart + reservedWords.PrefixStartValue + key + reservedWords.PrefixEnd,
                                        'g'),
                                        dict[tagName][i][key]);
    
                                }
    
                                //разбираемся с index-ом, если таковой имеется в цикле
                                loopBlockItem = loopBlockItem.replace(
                                        new RegExp(
                                            reservedWords.PrefixStart + reservedWords.PrefixStartValue + reservedWords.PrefixVariable +
                                            reservedWords.PrefixIndex + reservedWords.PrefixEnd,
                                        'g'),
                                        i + 1);
                                resultLoopBlock += loopBlockItem;
                            }
                        }
    
                        //заменяем в шаблоне блок цикла на значения, которые сформировались для этого цикла
                        tmpl = tmpl.substr(0, startIndex) +
                            resultLoopBlock +
                            tmpl.substr(endIndex + reservedWords.PrefixStart.length +
                            reservedWords.PrefixCloseStartLoop.length + " ".length + tagName.length +
                            reservedWords.PrefixCloseEndLoop.length + reservedWords.PrefixEnd.length);
                        
                        //обнуляем текущую позицию для поиска очередного цикла
                        currentIndex = 0;
                    }
                }
            }
        } while (currentIndex != -1);

        //заменяем все оставшиеся элементы (которые не циклы) в полученном шаблоне
        for (var key in dict) {
            //нас интересуют только те значения, которые не являются массивом
            if (typeof dict[key] == "string") {
                tmpl = tmpl.replace(
                    new RegExp(
                        reservedWords.PrefixStart + reservedWords.PrefixStartValue + key + reservedWords.PrefixEnd,
                    'g'),
                    dict[key]);
            }
        }

        return tmpl;
    }
    
    function isLoop(tag) {
        /// <summary>
        /// Определяем является ли тег тегом начала цикла
        /// </summary>
        /// <param name="tag">Тег</param>
        /// <returns type="boolean">true, false</returns>
        return tag.indexOf(reservedWords.PrefixOpenStartLoop)!=-1?true:false;
    }

    function getTagName(tag) {
        /// <summary>
        /// Функция определения имени тега, например, в теге {{each Deal}} это будет Deal
        /// </summary>
        /// <param name="tag">Тег</param>
        /// <returns type="string">Имя тега</returns>
        /// 
        // любое слово с $ в начале или без него
        var pattern = /\$?\w+/ig;
        var matches = tag.match(pattern);
        //в теге только 1 парамметр, остальное невалидно
        for (var i = 0; i < matches.length; i++) {
            var isReserve = false;
            for (var key in reservedWords) {
                if (reservedWords[key] == matches[i]) {
                    isReserve = true;
                    break;
                }
            }
            if (!isReserve) {
                return matches[i];
            }
        }
        return "";
    }
};